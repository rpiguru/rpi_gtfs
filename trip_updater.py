import urllib2
import time
from base import Base


class TripUpdater(Base):

    data = dict()
    last_modified = ''

    def __init__(self):
        Base.__init__(self)

    def update_schedule(self):
        """
        Download real-time trip update data and parse. (Downloaded data is based on Protocol Buffers format)
        :return:
        """
        if not self.debug:
            print "Started to download new schedule data..."
            s_time = time.time()
            # Send request and download zip file
            req = urllib2.Request(self.get_param_from_xml('API_URL'))
            req.add_header('authorization', self.get_param_from_xml('API_KEY'))
            req.add_header('cache-control', 'no-cache')
            try:
                resp = urllib2.urlopen(req)
            except:
                return False

            # Get last modified time
            last_modified = resp.info().getheader('Last-Modified')

            print "Last modified time: ", last_modified

            if self.last_modified == last_modified:
                print "Data is not updated yet..."
                return False

            self.last_modified = last_modified

            print 'Downloading attachment...'

            # try:
            content = resp.read()
            # except:
            #     return False

            open('tripupdate1.txt', 'wb').write(content)

            print "Download successfully, elapsed time: ", time.time() - s_time

            re = content.splitlines()

        else:
            re = open('tripupdate1.txt', 'r').readlines()

        print "Parsing data, length: ", len(re), '\n\n'

        data = dict()

        data.clear()
        data['timestamp'] = int(re[3].split(':')[1].strip())
        data['trip_list'] = []

        n = 5
        while 'entity' in re[n]:
            try:
                # Some entities has omitted trip_id, so ignore them...
                if 'trip_id' not in re[n+4]:
                    print "Invalid type of data, skipping..."
                    n += 1
                    while 'entity' not in re[n]:
                        n += 1
                    continue

                entity = dict()
                entity['trip_id'] = re[n+4].split('"')[1]
                entity['start_time'] = re[n+5].split('"')[1]
                entity['start_date'] = re[n+6].split('"')[1]
                entity['schedule_relationship'] = re[n+7].split()[-1]
                entity['route_id'] = re[n+8].split('"')[1]
                n += 10
            except IndexError:
                print "Out of index, seems to reached to the end of file... n=", n
                break

            entity['l_stop_time'] = []
            while 'stop_time_update' in re[n]:
                try:
                    stop_time = dict()
                    stop_time['stop_sequence'] = int(re[n+1].split()[-1])
                    stop_time['arrival_delay'] = int(re[n+3].split()[-1])
                    stop_time['arrival_time'] = int(re[n+4].split()[-1])
                    stop_time['departure_delay'] = int(re[n+7].split()[-1])
                    stop_time['departure_time'] = int(re[n+8].split()[-1])
                    stop_time['stop_id'] = re[n+10].split('"')[1]
                    stop_time['schedule_relationship'] = re[n+11].split()[-1]
                    entity['l_stop_time'].append(stop_time)
                    n += 13

                    tmp = re[n+11]  # check next index for the end of data
                except IndexError:
                    print "Out of index, seems to reached to the end of file, n=", n
                    break
                except ValueError as e:
                    print e
                    print n
                    break
            try:
                if 'vehicle' in re[n]:
                    entity['vehicle_id'] = re[n+1].split('"')[1]
                    n += 7
                else:
                    entity['vehicle_id'] = None
                    n += 2
                data['trip_list'].append(entity)

                tmp = re[n]     # Check for next data
            except IndexError:
                print "Out of index, seems to reached to the end of file, n=", n
                data['trip_list'].append(entity)
                break

        print 'Final line: ', n

        self.data = data
        return True

    def get_trips_of_route(self, route_id):
        """
        Get list of routes by route_id
        :param route_id:
        :return:
        """
        trip_list = []
        for trip in self.data['trip_list']:
            if trip['route_id'] == route_id:
                trip_list.append(trip)

        return trip_list

    def get_timetable_of_bus_stop(self, route_id, stop_id):
        trip_list = self.get_trips_of_route(route_id)

        tt_item = None
        for trip in trip_list:
            for b_stop in trip['l_stop_time']:
                if b_stop['stop_id'] == stop_id and b_stop['schedule_relationship'] == 'SCHEDULED':
                    tt_item = b_stop
                    tt_item['trip_id'] = trip['trip_id']

        return tt_item

    def get_all_routes(self, stop_id):
        tt_list = []
        for trip in self.data['trip_list']:
            for b_stop in trip['l_stop_time']:
                if b_stop['stop_id'] == stop_id and b_stop['schedule_relationship'] == 'SCHEDULED':
                    tt_item = b_stop
                    tt_item['trip_id'] = trip['trip_id']
                    tt_item['rout_id'] = trip['route_id']
                    tt_list.append(tt_item)

        return tt_list


if __name__ == '__main__':
    a = TripUpdater()

    while True:
        if a.update_schedule():
            time_table = a.get_timetable_of_bus_stop('2441_389', '200916')
            # time_table = a.get_all_routes('2770144')
            if time_table is not None:

                for k in time_table.keys():
                    print k, ': ', time_table[k]
                print 'Current time: ', int(time.time()), '\n'
            else:
                print "Data not found...\n\n"





